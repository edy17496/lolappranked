package com.carm.lolappcleanarchitecture.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.carm.lolappcleanarchitecture.ui.initSummoner.InitSummonerRouter
import com.carm.lolappcleanarchitecture.utils.view.makeStatusBarTransparent


class SplashActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		InitSummonerRouter().launch(this)
	}

	private fun initFields() {
		//makeStatusBarTransparent(true)
	}

}