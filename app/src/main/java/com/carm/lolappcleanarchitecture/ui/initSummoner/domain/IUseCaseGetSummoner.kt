package com.carm.lolappcleanarchitecture.ui.initSummoner.domain

import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import com.carm.lolappcleanarchitecture.utils.data.Lce

interface IUseCaseGetSummoner {
	suspend fun getSummoner(name: String): Lce<Summoner?>
}
