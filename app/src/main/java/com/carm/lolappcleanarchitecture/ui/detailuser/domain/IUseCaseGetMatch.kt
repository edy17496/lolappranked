package com.carm.lolappcleanarchitecture.ui.detailuser.domain

import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.utils.data.Lce

interface IUseCaseGetMatch {
	suspend fun getMatch(idMatch: String): Lce<GetMatch?>
}