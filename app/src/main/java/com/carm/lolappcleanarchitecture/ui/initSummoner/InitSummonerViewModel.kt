package com.carm.lolappcleanarchitecture.ui.initSummoner

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carm.fishingservice.utils.dispatcher.IDispatchersProvider
import com.carm.lolappcleanarchitecture.app.data.model.ErrorNotConnection
import com.carm.lolappcleanarchitecture.app.data.model.ErrorWithCode
import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import com.carm.lolappcleanarchitecture.app.data.preferences.IPreferenceProvider
import com.carm.lolappcleanarchitecture.app.data.preferences.PreferenceProvider
import com.carm.lolappcleanarchitecture.app.data.preferences.PreferencesKey
import com.carm.lolappcleanarchitecture.ui.initSummoner.InitSummonerActivity.StateViewInitSummoner
import com.carm.lolappcleanarchitecture.ui.initSummoner.domain.IUseCaseGetSummoner
import com.carm.lolappcleanarchitecture.utils.data.Lce
import kotlinx.coroutines.launch

class InitSummonerViewModel(
	private val dispatcher: IDispatchersProvider,
	private val preferenceProvider: IPreferenceProvider,
	private val useCaseGetSummoner: IUseCaseGetSummoner
) : ViewModel() {

	val stateView: LiveData<StateViewInitSummoner>
		get() = _stateView
	private val _stateView = MutableLiveData<StateViewInitSummoner>()


	fun postSummoner(name: String?) {
		viewModelScope.launch(dispatcher.default) {
			val response = useCaseGetSummoner.getSummoner(name!!)
			val stateView = getStateView(response)
			_stateView.postValue(stateView)
		}
	}

	private fun getStateView(response: Lce<Summoner?>): StateViewInitSummoner {
		return when (response) {
			is Lce.Content -> {
				if (response.packet != null) {
					preferenceProvider.set(PreferencesKey.SUMMONER, response.packet)
					StateViewInitSummoner.SuccessInitSummoner
				} else {
					StateViewInitSummoner.ErrorInitSummoner
				}
			}
			is Lce.Error -> {
				when (response.error) {
					is ErrorWithCode -> {
						StateViewInitSummoner.ErrorNotExitInitSummoner
					}
					is ErrorNotConnection -> {
						StateViewInitSummoner.ErrorInitSummoner
					}
					else -> {
						StateViewInitSummoner.ErrorInitSummoner
					}
				}
			}
			is Lce.Loading -> {
				StateViewInitSummoner.LoadInitSummoner
			}
		}
	}

	companion object {
		val TAG: String = InitSummonerViewModel::class.java.simpleName
	}
}