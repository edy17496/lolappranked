package com.carm.lolappcleanarchitecture.ui.initSummoner.domain

import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSource
import com.carm.lolappcleanarchitecture.utils.data.Lce

class GetSummonerService(
	private val dataSource: RetrofitDataSource
) : IUseCaseGetSummoner {
	override suspend fun getSummoner(name: String): Lce<Summoner?> {
		return dataSource.getSummoner(name)
	}
}