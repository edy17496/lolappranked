package com.carm.lolappcleanarchitecture.ui.initSummoner

import android.content.Context
import android.content.Intent
import com.carm.lolappcleanarchitecture.app.base.BaseActivity
import com.carm.lolappcleanarchitecture.app.base.BaseActivityRouter

class InitSummonerRouter: BaseActivityRouter {

	override fun intent(activity: Context): Intent = Intent(activity, InitSummonerActivity::class.java)
}