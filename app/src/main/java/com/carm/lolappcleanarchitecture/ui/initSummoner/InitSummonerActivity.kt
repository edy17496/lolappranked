package com.carm.lolappcleanarchitecture.ui.initSummoner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.carm.lolappcleanarchitecture.R
import com.carm.lolappcleanarchitecture.databinding.ActivityInitSummonerBinding
import com.carm.lolappcleanarchitecture.ui.detailuser.DetailUserRouter
import com.carm.lolappcleanarchitecture.utils.view.makeStatusBarTransparent
import com.carm.lolappcleanarchitecture.utils.view.makeToast
import org.koin.androidx.viewmodel.ext.android.viewModel


class InitSummonerActivity : AppCompatActivity() {

	private lateinit var binding: ActivityInitSummonerBinding
	private val viewModel: InitSummonerViewModel by viewModel()


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, R.layout.activity_init_summoner)
		initFields()
		initListeners()
		initObservers()
	}

	private fun initFields() {
		makeStatusBarTransparent(false)
	}

	private fun initListeners() {
		binding.btnSendName.setOnClickListener {
			viewModel.postSummoner(binding.etSummonerName.text.toString())
		}
	}

	private fun initObservers(){
		viewModel.stateView.observe(this, {
			when(it){
				StateViewInitSummoner.LoadInitSummoner -> {

				}
				StateViewInitSummoner.SuccessInitSummoner -> {
					DetailUserRouter().launch(this)
				}
				StateViewInitSummoner.ErrorInitSummoner -> {
					makeToast(getString(R.string.user_not_exist))
				}
				StateViewInitSummoner.ErrorNotExitInitSummoner -> {
					makeToast(getString(R.string.user_not_exist))

				}
			}
		})
	}

	sealed class StateViewInitSummoner {
		object LoadInitSummoner : StateViewInitSummoner()
		object SuccessInitSummoner : StateViewInitSummoner()
		object ErrorNotExitInitSummoner : StateViewInitSummoner()
		object ErrorInitSummoner : StateViewInitSummoner()
	}

}