package com.carm.lolappcleanarchitecture.ui.detailuser

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carm.lolappcleanarchitecture.R
import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import com.carm.lolappcleanarchitecture.databinding.ActivityDetailUserBinding
import com.carm.lolappcleanarchitecture.ui.adapters.MatchesAdapter
import com.carm.lolappcleanarchitecture.ui.detailuser.DetailUserActivity.StateViewDetailUser.LoadDetailUser
import com.carm.lolappcleanarchitecture.utils.view.loadIcon
import com.carm.lolappcleanarchitecture.utils.view.makeToast
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailUserActivity : AppCompatActivity() {

	private lateinit var binding: ActivityDetailUserBinding
	private val viewModel: DetailUserViewModel by viewModel()

	private lateinit var adapterMatches: MatchesAdapter
	private lateinit var layoutManagerRecyclerView: LinearLayoutManager
	private var isLoadingNext = false


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_user)
		initFields()
		initListeners()
		initObservers()
		viewModel.initSummoner()
		viewModel.initMatches()
	}

	private fun initFields() {
		//makeStatusBarTransparent(false)
	}

	private fun initListeners() {

	}

	private fun initObservers() {
		viewModel.stateView.observe(this, {
			when (it) {
				LoadDetailUser -> {
					binding.pgLoading.visibility = View.VISIBLE
				}
				StateViewDetailUser.LoadListMatches -> {
					binding.pgLoading.visibility = View.VISIBLE
				}
				StateViewDetailUser.LoadNextListMatches -> {
					isLoadingNext = true
					binding.pgLoading.visibility = View.VISIBLE
				}

				is StateViewDetailUser.SuccessDetailUser -> {
					binding.ivProfile.loadIcon(this, it.summoner.profileIconId)
					binding.tvSummmonerName.text = it.summoner.name
					binding.tvSummonerLvl.text = it.summoner.summonerLevel.toString()
					isLoadingNext = false
				}
				is StateViewDetailUser.SuccessListMatchesDetailUser -> {
					initRecyclerView()
					adapterMatches.setMatches(it.matches, it.summonerId)
					binding.pgLoading.visibility = View.GONE
				}
				is StateViewDetailUser.SuccessNextListMatchesDetailUser -> {
					adapterMatches.addMatches(it.matches)
					isLoadingNext = false
					binding.pgLoading.visibility = View.GONE
				}

				StateViewDetailUser.ErrorDetailUser -> {
					binding.pgLoading.visibility = View.GONE
					makeToast(R.string.error_has_occurred)
				}
				StateViewDetailUser.ErrorListMatchesDetailUser -> {
					binding.pgLoading.visibility = View.GONE
					makeToast(R.string.error_has_occurred)
				}
				StateViewDetailUser.ErrorNextListMatchesDetailUser -> {
					binding.pgLoading.visibility = View.GONE
					makeToast(R.string.error_has_occurred)
				}
			}
		})
	}

	private fun initRecyclerView() {
		adapterMatches = MatchesAdapter()
		binding.rvMatches.apply {
			layoutManagerRecyclerView = LinearLayoutManager(context)
			layoutManagerRecyclerView.orientation = LinearLayoutManager.VERTICAL
			layoutManager = layoutManagerRecyclerView
			setHasFixedSize(true)
			adapter = adapterMatches
			addOnScrollListener(object : RecyclerView.OnScrollListener() {
				override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
					super.onScrolled(recyclerView, dx, dy)
					if (adapter?.itemCount != 0 && !recyclerView.canScrollVertically(1) && !isLoadingNext) {
						isLoadingNext = true
						viewModel.getNextMatches()
					}
				}
			})

		}
	}

	sealed class StateViewDetailUser {
		object LoadDetailUser : StateViewDetailUser()
		object LoadListMatches : StateViewDetailUser()
		object LoadNextListMatches : StateViewDetailUser()

		data class SuccessDetailUser(val summoner: Summoner) : StateViewDetailUser()

		data class SuccessListMatchesDetailUser(
			val matches: List<GetMatch>,
			val summonerId: String
		) :
			StateViewDetailUser()

		data class SuccessNextListMatchesDetailUser(
			val matches: List<GetMatch>,
			val summonerId: String
		) :
			StateViewDetailUser()

		object ErrorDetailUser : StateViewDetailUser()
		object ErrorListMatchesDetailUser : StateViewDetailUser()
		object ErrorNextListMatchesDetailUser : StateViewDetailUser()
	}
}