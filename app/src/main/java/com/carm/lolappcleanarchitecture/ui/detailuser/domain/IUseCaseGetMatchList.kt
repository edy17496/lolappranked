package com.carm.lolappcleanarchitecture.ui.detailuser.domain

import com.carm.lolappcleanarchitecture.utils.data.Lce

interface IUseCaseGetMatchList {

	suspend fun getMatchList(puuid: String, query: Map<String, String>): Lce<List<String>?>

	suspend fun getNextMatchList(puuid: String, query: Map<String, String>): Lce<List<String>?>

}