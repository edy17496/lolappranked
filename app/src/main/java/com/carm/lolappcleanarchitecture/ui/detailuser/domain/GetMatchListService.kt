package com.carm.lolappcleanarchitecture.ui.detailuser.domain

import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSourceEurope
import com.carm.lolappcleanarchitecture.utils.data.Lce

class GetMatchListService(
	private val dataSource: RetrofitDataSourceEurope
) : IUseCaseGetMatchList {

	override suspend fun getMatchList(
		puuid: String,
		query: Map<String, String>
	): Lce<List<String>?> {
		return dataSource.getListMatches(puuid, query)
	}

	override suspend fun getNextMatchList(
		puuid: String,
		query: Map<String, String>
	): Lce<List<String>?> {
		return dataSource.getNextListMatches(puuid, query)
	}
}