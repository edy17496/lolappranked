package com.carm.lolappcleanarchitecture.ui.detailuser.domain

import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSource
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSourceEurope
import com.carm.lolappcleanarchitecture.utils.data.BaseDataSource
import com.carm.lolappcleanarchitecture.utils.data.Lce

class GetMatchService(
	private val dataSource: RetrofitDataSourceEurope
): IUseCaseGetMatch {
	override suspend fun getMatch(idMatch: String): Lce<GetMatch?> {
		return dataSource.getMatch(idMatch)
	}
}