package com.carm.lolappcleanarchitecture.ui.detailuser

import androidx.lifecycle.*
import com.carm.fishingservice.utils.dispatcher.IDispatchersProvider
import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.app.data.preferences.IPreferenceProvider
import com.carm.lolappcleanarchitecture.app.data.preferences.PreferencesKey
import com.carm.lolappcleanarchitecture.ui.detailuser.DetailUserActivity.StateViewDetailUser
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.IUseCaseGetMatch
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.IUseCaseGetMatchList
import com.carm.lolappcleanarchitecture.utils.data.Lce
import kotlinx.coroutines.launch

class DetailUserViewModel(
	private val dispatcher: IDispatchersProvider,
	private val preferenceProvider: IPreferenceProvider,
	private val useCaseGetMatchList: IUseCaseGetMatchList,
	private val useCaseGetMatch: IUseCaseGetMatch,
) : ViewModel() {

	val stateView: LiveData<StateViewDetailUser>
		get() = _stateView
	private val _stateView = MutableLiveData<StateViewDetailUser>()

	private var start = 0


	fun initSummoner() {
		viewModelScope.launch(dispatcher.default) {
			val summoner = preferenceProvider.summoner(PreferencesKey.SUMMONER)
			summoner?.let {
				_stateView.postValue(StateViewDetailUser.SuccessDetailUser(it))
			} ?: run {
				_stateView.postValue(StateViewDetailUser.ErrorDetailUser)
			}
		}
	}

	fun initMatches() {
		_stateView.value = StateViewDetailUser.LoadListMatches
		viewModelScope.launch(dispatcher.default) {
			val puuid = preferenceProvider.summoner(PreferencesKey.SUMMONER)?.puuid ?: ""
			val summonerId = preferenceProvider.summoner(PreferencesKey.SUMMONER)?.id ?: ""
			val query = HashMap<String, String>()
			query["count"] = COUNT.toString()
			when (val responseListMatches = useCaseGetMatchList.getMatchList(puuid, query)) {
				is Lce.Content -> {
					if (responseListMatches.packet != null) {
						start = responseListMatches.packet.size
						val matches = ArrayList<GetMatch>()
						for (idMatch in responseListMatches.packet) {
							val match = useCaseGetMatch.getMatch(idMatch)
							if (match is Lce.Content && match.packet != null) {
								matches.add(match.packet)
							}
						}

						_stateView.postValue(
							StateViewDetailUser.SuccessListMatchesDetailUser(
								matches,
								summonerId
							)
						)
					}

				}
				is Lce.Error -> {
					_stateView.postValue(StateViewDetailUser.ErrorListMatchesDetailUser)
				}
				is Lce.Loading -> {

				}
			}
		}
	}

	fun getNextMatches() {
		_stateView.value = StateViewDetailUser.LoadNextListMatches
		viewModelScope.launch(dispatcher.default) {
			val puuid = preferenceProvider.summoner(PreferencesKey.SUMMONER)?.puuid ?: ""
			val summonerId = preferenceProvider.summoner(PreferencesKey.SUMMONER)?.id ?: ""
			val query = HashMap<String, String>()
			query["start"] = start.toString()
			query["count"] = COUNT.toString()
			when (val responseListMatches = useCaseGetMatchList.getNextMatchList(puuid, query)) {
				is Lce.Content -> {
					if (responseListMatches.packet != null) {
						start += responseListMatches.packet.size
						val matches = ArrayList<GetMatch>()
						for (idMatch in responseListMatches.packet) {
							val match = useCaseGetMatch.getMatch(idMatch)
							if (match is Lce.Content && match.packet != null) {
								matches.add(match.packet)
							}
						}

						_stateView.postValue(
							StateViewDetailUser.SuccessNextListMatchesDetailUser(
								matches,
								summonerId
							)
						)
					}

				}
				is Lce.Error -> {
					_stateView.postValue(StateViewDetailUser.ErrorNextListMatchesDetailUser)
				}
				is Lce.Loading -> {

				}
			}
		}
	}

	companion object {
		const val COUNT = 8
	}
}
