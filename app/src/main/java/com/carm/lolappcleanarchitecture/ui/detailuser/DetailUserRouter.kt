package com.carm.lolappcleanarchitecture.ui.detailuser

import android.content.Context
import android.content.Intent
import com.carm.lolappcleanarchitecture.app.base.BaseActivityRouter

class DetailUserRouter : BaseActivityRouter {
	override fun intent(activity: Context): Intent =
		Intent(activity, DetailUserActivity::class.java)
}