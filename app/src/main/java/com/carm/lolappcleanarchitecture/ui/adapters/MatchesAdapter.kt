package com.carm.lolappcleanarchitecture.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.carm.lolappcleanarchitecture.R
import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.databinding.ItemMatchBinding
import com.carm.lolappcleanarchitecture.utils.view.loadChampion

class MatchesAdapter : RecyclerView.Adapter<MatchesAdapter.ViewHolder>() {

	private var summonerId: String = ""

	private var context: Context? = null
	private var items: ArrayList<GetMatch> = ArrayList()

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val binding: ItemMatchBinding = DataBindingUtil.inflate(
			LayoutInflater.from(parent.context),
			R.layout.item_match,
			parent,
			false
		)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val match = items[position]
		holder.bind(match)
	}

	override fun getItemCount(): Int {
		return items.size
	}

	fun setMatches(matches: List<GetMatch>, summonerId: String) {
		items.clear()
		val size = matches.size
		items.addAll(matches)
		this.summonerId = summonerId
		notifyItemRangeInserted(0, size)

	}

	fun addMatches(matches: List<GetMatch>) {
		val size = items.size
		items.addAll(matches)
		notifyItemRangeInserted(size, matches.size)

	}

	inner class ViewHolder(private val binding: ItemMatchBinding) :
		RecyclerView.ViewHolder(binding.root) {


		fun bind(match: GetMatch?) {

			match?.info?.gameMode?.let {
				binding.tvTypeMatch.text = it
			}

			// Comprobar cuando es win o defeat
			val ownerParticipant = match?.info?.participants?.filter { participantsItem ->
				participantsItem.summonerId == summonerId
			}

			if (ownerParticipant?.isNotEmpty() == true) {
				val summoner = ownerParticipant[0]
				when (summoner.win) {
					true -> {
						binding.clMain.setBackgroundColor(
							ContextCompat.getColor(
								context!!,
								R.color.background_win
							)
						)
						binding.tvWin.text = context!!.getString(R.string.win)
						binding.tvWin.setTextColor(
							ContextCompat.getColor(
								context!!,
								R.color.text_color_win
							)
						)
						binding.ivChampion.borderColor = ContextCompat.getColor(
							context!!,
							R.color.text_color_win
						)
					}

					false -> {
						binding.clMain.setBackgroundColor(
							ContextCompat.getColor(
								context!!,
								R.color.background_loss
							)
						)
						binding.tvWin.text = context!!.getString(R.string.loss)
						binding.tvWin.setTextColor(
							ContextCompat.getColor(
								context!!,
								R.color.text_color_loss
							)
						)
						binding.ivChampion.borderColor = ContextCompat.getColor(
							context!!,
							R.color.text_color_loss
						)
					}
				}

				val kills = summoner.kills
				val deaths = summoner.deaths
				val assists = summoner.assists
				val kdaRatio = (kills + assists) / 1.coerceAtLeast(deaths)
				binding.tvKda.text = "$kills/$deaths/$assists"
				binding.tvKdaScore.text = kdaRatio.toString()
				binding.ivChampion.loadChampion(context!!,summoner.championName)
			}

		}

	}
}