package com.carm.lolappcleanarchitecture.utils.view

import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.DialogFragment

fun DialogFragment.makeStatusBarTransparent(isBlackIcon: Boolean): Boolean {
	dialog?.window.apply {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			this?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
			this?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
			this?.statusBarColor = Color.TRANSPARENT
			return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if (isBlackIcon) {
					this?.decorView?.systemUiVisibility =
						View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
				} else {
					this?.decorView?.systemUiVisibility =
						View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				}
				true
			} else {
				this?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				false
			}

		}
		return false
	}
}