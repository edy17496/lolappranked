package com.carm.fishingservice.utils.dispatcher

import com.carm.fishingservice.utils.dispatcher.IDispatchersProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineDispatcher

class TestDispatchers : IDispatchersProvider {

	override val main: CoroutineDispatcher
		get() = TestCoroutineDispatcher()

	override val io: CoroutineDispatcher
		get() = TestCoroutineDispatcher()

	override val default: CoroutineDispatcher
		get() = TestCoroutineDispatcher()

	override val unconfined: CoroutineDispatcher
		get() = TestCoroutineDispatcher()
}