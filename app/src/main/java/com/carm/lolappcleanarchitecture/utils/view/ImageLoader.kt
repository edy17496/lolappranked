package com.carm.lolappcleanarchitecture.utils.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.Headers
import com.bumptech.glide.request.RequestOptions
import com.carm.lolappcleanarchitecture.R
import retrofit2.http.Url
import java.io.File
import java.net.URL
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.squareup.picasso.Picasso
import java.util.*


fun ImageView.loadImageWithoutDefault(context: Context, url: String) {
	Glide.with(context)
		.load(url)
		.diskCacheStrategy(DiskCacheStrategy.NONE)
		.into(this)
}

fun ImageView.loadIconWithoutDefault(context: Context, iconId: Int) {
	val options = RequestOptions()
		.centerCrop()
		.placeholder(R.drawable.lol_icon)
		.error(R.drawable.lol_icon)
	val urlIcon = "http://ddragon.leagueoflegends.com/cdn/12.2.1/img/profileicon/$iconId.png"
	Glide.with(context)
		.asDrawable()
		.load(urlIcon)
		.into(this)
}

fun ImageView.loadIcon(context: Context, iconId: Int) {
	val resource = context.resources
	val resourceId = resource.getIdentifier("summoner_icon$iconId", "drawable", context.packageName)
	if (resourceId == 0) {
		this.setImageResource(R.drawable.lol_icon)
	} else {
		this.setImageResource(resourceId)
	}
}

fun ImageView.loadChampion(context: Context, iconId: String) {
	val resource = context.resources
	val resourceId = resource.getIdentifier(
		"champion_${iconId.lowercase()}",
		"drawable",
		context.packageName
	)
	if (resourceId == 0) {
		this.setImageResource(R.drawable.lol_icon)
	} else {
		this.setImageResource(resourceId)
	}
}

fun ImageView.loadImageWithoutDefaultFromFilePath(context: Context, pathFile: String) {
	val file = File(pathFile)
	val uri = Uri.fromFile(file)
	Glide.with(context)
		.load(uri)
		.diskCacheStrategy(DiskCacheStrategy.NONE)
		.into(this)
}