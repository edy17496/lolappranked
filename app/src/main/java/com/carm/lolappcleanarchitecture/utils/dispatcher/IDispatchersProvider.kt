package com.carm.fishingservice.utils.dispatcher

import kotlinx.coroutines.CoroutineDispatcher

interface IDispatchersProvider {
	val main: CoroutineDispatcher
	val io: CoroutineDispatcher
	val default: CoroutineDispatcher
	val unconfined: CoroutineDispatcher
}