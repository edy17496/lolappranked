package com.carm.lolappcleanarchitecture.utils.view

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.view.WindowManager
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout

fun Activity.makeStatusBarTransparent(isBlackIcon: Boolean): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = Color.TRANSPARENT
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isBlackIcon) {
                    decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                }
                true
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                false
            }
        }
    }
    return false
}

fun View.setMargins(marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int) {
    val menuLayoutParams = this.layoutParams as MarginLayoutParams
    menuLayoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom)
    this.layoutParams = menuLayoutParams
}

fun ConstraintLayout.setMargins(marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int) {
    val newLayoutParams = this.layoutParams as ConstraintLayout.LayoutParams
    newLayoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom)
    this.layoutParams = newLayoutParams
}

fun Context.makeToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.makeToast(message: Int) {
    Toast.makeText(this, getText(message), Toast.LENGTH_SHORT).show()
}
