package com.carm.lolappcleanarchitecture

import android.app.Application
import com.carm.lolappcleanarchitecture.app.di.moduleKoin
import com.carm.lolappcleanarchitecture.app.di.useCase
import com.carm.lolappcleanarchitecture.app.di.viewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class LeagueOfLegendsService : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(applicationContext)
            modules(moduleKoin, useCase, viewModel)
        }
    }
}