package com.carm.lolappcleanarchitecture.app.data.model

data class Objectives(val baron: Baron,
                      val inhibitor: Inhibitor,
                      val dragon: Dragon,
                      val riftHerald: RiftHerald,
                      val champion: Champion,
                      val tower: Tower)