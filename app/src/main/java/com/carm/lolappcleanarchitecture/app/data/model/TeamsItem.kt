package com.carm.lolappcleanarchitecture.app.data.model

data class TeamsItem(val teamId: Int = 0,
                     val bans: List<BansItem>?,
                     val objectives: Objectives,
                     val win: Boolean = false)