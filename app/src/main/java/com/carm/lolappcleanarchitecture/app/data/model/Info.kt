package com.carm.lolappcleanarchitecture.app.data.model

data class Info(val gameId: Long = 0,
                val gameType: String = "",
                val queueId: Int = 0,
                val gameDuration: Int = 0,
                val teams: List<TeamsItem>?,
                val gameEndTimestamp: Long = 0,
                val gameStartTimestamp: Long = 0,
                val platformId: String = "",
                val gameCreation: Long = 0,
                val gameName: String = "",
                val tournamentCode: String = "",
                val gameVersion: String = "",
                val mapId: Int = 0,
                val gameMode: String = "",
                val participants: List<ParticipantsItem>?)