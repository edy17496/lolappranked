package com.carm.lolappcleanarchitecture.app.data.model

data class Metadata(val dataVersion: String = "",
                    val matchId: String = "",
                    val participants: List<String>?)