package com.carm.lolappcleanarchitecture.app.data.model

data class BansItem(val championId: Int = 0,
                    val pickTurn: Int = 0)