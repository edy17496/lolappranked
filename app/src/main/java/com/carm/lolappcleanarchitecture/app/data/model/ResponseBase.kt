package com.carm.lolappcleanarchitecture.app.data.model

open class ResponseBase (
	val response: String
) {
	constructor() : this("")
}