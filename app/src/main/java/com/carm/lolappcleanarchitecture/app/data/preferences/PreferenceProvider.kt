package com.carm.lolappcleanarchitecture.app.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.carm.lolappcleanarchitecture.R
import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import com.google.gson.Gson


class PreferenceProvider(
    private val context: Context
) : IPreferenceProvider {

    override fun set(key: PreferencesKey, value: String) {
        val editor = prefs().edit()
        editor.putString(key.value, value).apply()
    }

    override fun string(key: PreferencesKey): String? {
        return prefs().getString(key.value, null)
    }

    override fun long(key: PreferencesKey): Long {
        return prefs().getLong(key.value, 0)
    }

    override fun set(key: PreferencesKey, value: Long) {
        val editor = prefs().edit()
        editor.putLong(key.value, value).apply()
    }

    override fun bool(key: PreferencesKey): Boolean? {
        return prefs().getBoolean(key.value, false)
    }

    override fun set(key: PreferencesKey, value: Boolean) {
        val editor = prefs().edit()
        editor.putBoolean(key.value, value).apply()
    }

    override fun remove(key: PreferencesKey) {
        val editor = prefs().edit()
        editor.remove(key.value).apply()
    }

    override fun set(key: PreferencesKey, value: Summoner) {
        val editor = prefs().edit()
        editor.putString(key.value, Gson().toJson(value)).apply()
    }

    override fun summoner(key: PreferencesKey): Summoner? {
        return try {
            Gson().fromJson(string(key), Summoner::class.java)
        } catch (e: Exception) {
            null
        }
    }

    override fun clear() {
        val editor = prefs().edit()
        editor.clear().apply()
    }

    private fun prefs(): SharedPreferences {
        return context.getSharedPreferences(
            context.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
    }
}

enum class PreferencesKey(val value: String) {
    ID_SESSION("id_summoner"),
    SUMMONER("summoner"),
    IS_LOGIN("is_login")
}