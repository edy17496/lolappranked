package com.carm.lolappcleanarchitecture.app.di

import com.carm.lolappcleanarchitecture.app.data.logger.IPrintLogger
import com.carm.lolappcleanarchitecture.app.data.logger.ImplementPrintLogger
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.NetworkConnectionInterceptor
import com.carm.fishingservice.utils.dispatcher.IDispatchersProvider
import com.carm.fishingservice.utils.dispatcher.StandardDispatchers
import com.carm.lolappcleanarchitecture.app.data.preferences.IPreferenceProvider
import com.carm.lolappcleanarchitecture.app.data.preferences.PreferenceProvider
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.AuthInterceptor
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSource
import com.carm.lolappcleanarchitecture.app.data.source.retrofit.RetrofitDataSourceEurope
import com.carm.lolappcleanarchitecture.ui.detailuser.DetailUserViewModel
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.GetMatchListService
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.GetMatchService
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.IUseCaseGetMatch
import com.carm.lolappcleanarchitecture.ui.detailuser.domain.IUseCaseGetMatchList
import com.carm.lolappcleanarchitecture.ui.initSummoner.InitSummonerViewModel
import com.carm.lolappcleanarchitecture.ui.initSummoner.domain.GetSummonerService
import com.carm.lolappcleanarchitecture.ui.initSummoner.domain.IUseCaseGetSummoner
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@JvmField
val moduleKoin = module {
	single<IDispatchersProvider> { StandardDispatchers() }
	single<IPrintLogger> { ImplementPrintLogger() }
	single { AuthInterceptor() }
	single { RetrofitDataSource(get()) }
	single { RetrofitDataSourceEurope(get()) }
	single { NetworkConnectionInterceptor(androidContext()) }
	single<IPreferenceProvider> { PreferenceProvider(androidContext()) }
}

@JvmField
val useCase = module {
	single<IUseCaseGetSummoner> { GetSummonerService(get()) }
	single<IUseCaseGetMatchList> { GetMatchListService(get()) }
	single<IUseCaseGetMatch> { GetMatchService(get()) }

}

@JvmField
val viewModel = module {
	viewModel { InitSummonerViewModel(get(), get(), get()) }
	viewModel { DetailUserViewModel(get(), get(), get(), get()) }
}