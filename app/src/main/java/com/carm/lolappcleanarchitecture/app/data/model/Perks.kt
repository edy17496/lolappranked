package com.carm.lolappcleanarchitecture.app.data.model

data class Perks(val statPerks: StatPerks,
                 val styles: List<StylesItem>?)