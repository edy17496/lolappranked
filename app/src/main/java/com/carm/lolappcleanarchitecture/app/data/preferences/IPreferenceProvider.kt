package com.carm.lolappcleanarchitecture.app.data.preferences

import com.carm.lolappcleanarchitecture.app.data.model.Summoner

interface IPreferenceProvider {

	fun set(key: PreferencesKey, value: String)
	fun string(key: PreferencesKey): String?
	fun set(key: PreferencesKey, value: Long)
	fun long(key: PreferencesKey): Long?
	fun set(key: PreferencesKey, value: Boolean)
	fun bool(key: PreferencesKey): Boolean?
	fun remove(key: PreferencesKey)
	fun set(key: PreferencesKey, value: Summoner)
	fun summoner(key: PreferencesKey): Summoner?
	fun clear()
}