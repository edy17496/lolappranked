package com.carm.lolappcleanarchitecture.app.data.source.retrofit

import com.carm.lolappcleanarchitecture.app.data.model.GetMatch
import com.carm.lolappcleanarchitecture.app.data.model.Summoner
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface IApiService {

	@GET("/lol/summoner/v4/summoners/by-name/{summonerName}")
	suspend fun getSummoner(@Path("summonerName") summonerName: String): Response<Summoner>

	@GET("/lol/match/v5/matches/by-puuid/{puuid}/ids")
	suspend fun getListMatches(
		@Path("puuid") puuid: String,
		@QueryMap query: Map<String, String>
	): Response<List<String>>

	@GET("/lol/match/v5/matches/by-puuid/{puuid}/ids")
	suspend fun getNextListMatches(
		@Path("puuid") puuid: String,
		@QueryMap query: Map<String, String>
	): Response<List<String>>

	@GET("/lol/match/v5/matches/{matchId}")
	suspend fun getMatch(@Path("matchId") matchId: String): Response<GetMatch>
}