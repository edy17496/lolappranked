package com.carm.lolappcleanarchitecture.app.data.model

data class Summoner(
	val accountId: String = "",
	val profileIconId: Int = 0,
	val revisionDate: Long = 0,
	val name: String = "",
	val puuid: String = "",
	val id: String = "",
	val summonerLevel: Int = 0
)