package com.carm.fishingservice.app.data.source.remote.retrofit

import java.io.IOException

class NoConnectivityException : IOException() {

    override fun getLocalizedMessage(): String {
        return "No Internet Connection"
    }
}
