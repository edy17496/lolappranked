package com.carm.lolappcleanarchitecture.app.data.source.retrofit

import com.carm.lolappcleanarchitecture.app.data.logger.IPrintLogger
import com.carm.lolappcleanarchitecture.BuildConfig
import com.carm.lolappcleanarchitecture.utils.data.BaseDataSource
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.java.KoinJavaComponent.inject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitDataSourceEurope(printLogger: IPrintLogger) {

	private var apiService: IApiService? = null
	private var baseData: BaseDataSource? = null

	init {
		this.baseData = BaseDataSource(printLogger)
		initRetrofit()
	}

	private fun initRetrofit() {

		val authInterceptor: AuthInterceptor by inject(clazz = AuthInterceptor::class.java)
		val networkConnectionInterceptor: NetworkConnectionInterceptor by inject(clazz = NetworkConnectionInterceptor::class.java)

		val gson = GsonBuilder().setLenient().create()
		val logging = createLoggingInterceptor()

		val client: OkHttpClient = OkHttpClient.Builder()
			.addInterceptor(authInterceptor)
			.addInterceptor(networkConnectionInterceptor)
			.addInterceptor(logging)
			.readTimeout(30, TimeUnit.SECONDS)
			.writeTimeout(30, TimeUnit.SECONDS)
			.connectTimeout(30, TimeUnit.SECONDS)
			.retryOnConnectionFailure(true)
			.build()
		val retrofit = Retrofit.Builder()
			.baseUrl(BuildConfig.API_URL_EUROPE)
			.client(client)
			.addConverterFactory(GsonConverterFactory.create(gson))
			.build()
		apiService = retrofit.create(IApiService::class.java)
	}

	private fun createLoggingInterceptor(): HttpLoggingInterceptor {
		val logging = HttpLoggingInterceptor()
		logging.level = HttpLoggingInterceptor.Level.BODY
		logging.level = if (BuildConfig.DEBUG) {
			HttpLoggingInterceptor.Level.BODY
		} else {
			HttpLoggingInterceptor.Level.NONE
		}
		return logging
	}

	suspend fun getListMatches(puuid: String, query: Map<String, String>) =
		baseData!!.getResult { apiService!!.getListMatches(puuid, query) }

	suspend fun getNextListMatches(puuid: String, query: Map<String, String>) =
		baseData!!.getResult { apiService!!.getNextListMatches(puuid, query) }

	suspend fun getMatch(matchId: String) =
		baseData!!.getResult { apiService!!.getMatch(matchId) }
}

