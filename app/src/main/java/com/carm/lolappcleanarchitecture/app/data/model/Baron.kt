package com.carm.lolappcleanarchitecture.app.data.model

data class Baron(val kills: Int = 0,
                 val first: Boolean = false)