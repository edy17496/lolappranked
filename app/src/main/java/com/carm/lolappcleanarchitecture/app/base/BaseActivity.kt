package com.carm.lolappcleanarchitecture.app.base

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {


    companion object {
        val TAG: String = BaseActivity::class.java.simpleName
    }
}