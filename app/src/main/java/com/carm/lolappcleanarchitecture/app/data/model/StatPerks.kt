package com.carm.lolappcleanarchitecture.app.data.model

data class StatPerks(val offense: Int = 0,
                     val defense: Int = 0,
                     val flex: Int = 0)