package com.carm.lolappcleanarchitecture.app.base

import androidx.fragment.app.DialogFragment
import android.graphics.Point

import android.os.Build


abstract class BaseFragmentDialog : DialogFragment() {

    fun getWithScreen(): Int {
        var point = 0
        dialog?.let {
            point = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val window = it.window
                window!!.windowManager.currentWindowMetrics.bounds.width()
            } else {
                val size = Point()
                activity?.windowManager?.defaultDisplay?.getSize(size)
                size.x
            }
        }
        return point
    }
}