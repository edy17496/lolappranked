package com.carm.lolappcleanarchitecture.app.data.model

data class Tower(val kills: Int = 0,
                 val first: Boolean = false)