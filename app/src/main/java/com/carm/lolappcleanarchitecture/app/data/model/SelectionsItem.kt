package com.carm.lolappcleanarchitecture.app.data.model

data class SelectionsItem(val perk: Int = 0,
                          val var1: Int = 0,
                          val var2: Int = 0,
                          val var3: Int = 0)