package com.carm.lolappcleanarchitecture.app.data.logger

interface IPrintLogger {
	fun printError(error: String)
	fun printError(error: Throwable)
}