package com.carm.lolappcleanarchitecture.app.data.model

data class GetMatch(val metadata: Metadata,
                    val info: Info)