package com.carm.lolappcleanarchitecture.app.data.source.retrofit

import com.carm.lolappcleanarchitecture.BuildConfig
import com.carm.lolappcleanarchitecture.app.data.preferences.IPreferenceProvider
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class AuthInterceptor : Interceptor {

	override fun intercept(chain: Interceptor.Chain): Response {
		var request: Request = chain.request()
		val url: HttpUrl =
			request.url.newBuilder().addQueryParameter("api_key", BuildConfig.TOKEN_API).build()
		request = request.newBuilder().url(url).build()
		return chain.proceed(request)
	}

}