package com.carm.lolappcleanarchitecture.app.data.model

data class StylesItem(val selections: List<SelectionsItem>?,
                      val description: String = "",
                      val style: Int = 0)