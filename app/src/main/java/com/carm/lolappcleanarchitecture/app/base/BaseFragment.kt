package com.carm.lolappcleanarchitecture.app.base

import androidx.fragment.app.Fragment

class BaseFragment: Fragment() {

    companion object {
        val TAG: String = BaseFragment::class.java.simpleName
    }
}