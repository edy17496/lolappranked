# LOLMatch
## _Aplicación Android con Clean Architecture_ 

Pequeña aplicación donde se utilizan distintas herramientas actuales para el desarrollo de una aplicación Android. Algunas de estás aplicaciones android son las siguientes:

## Tecnologías

- **Kotlin**: Aplicación escrita completamente con Kotlin.
- **Retrofit**: Liberia utilizada para consumir los servicios de la API de Riot Games.
- **API Riot Games**: Usada para consumir los distintos datos que ofrece esta API.
- **Corutinas**: Al tratarse de una aplicación Kotlin frendly se usa todas las utilidades que ofrece este lenguaje. 
- **Koin**: Liberia usada para la inyección de dependencia.
- **Git**: Para el control de versiones.



## Arquitectura y patrones 
![Screenshot](https://uploads.toptal.io/blog/image/127608/toptal-blog-image-1543413671794-80993a19fea97477524763c908b50a7a.png)
>Diagrama MVVM con mejoras de módulos. Abhishek Tyagi (2018),Better Android Apps Using MVVM with Clean Architecture Extraido de: https://www.toptal.com/
- **MVI** *(Model View Intent)*: Es una arquitectura similar a una arquitectura MVVM *(ModelViewViewModel)* donde la principal diferencia se encuentra en la comunicación entre la *"View"* y el *"ViewModel"*. En una arquitectura MVI los datos necesarios para para la capa de la UI se emiten mediante *"LiveData"* para posteriormente ser mostrados al usuario, mientras que en una arquitectura MVVM dichos datos son enlazados desde el ViewModel al fichero *"XML"* asociado. 
- **UDF** *(Unidirectional Data Flow)*: Para la comunicación entre la view y el viewModel se sigue un patrón UDF. Algunas de las ventajas que presenta la implementación de este patrón son las siguientes:    
    - Control de la UI y eventos asíncronos.
    - Debug de manera fácil y sencilla.  
Esta estructura mantiene 3 elementos claves:
    - *View State:* Sirve para comunicar estados de datos persistentes que serán mostrados al usuario por medio de la UI. Por ejemplo al recuperar los datos de una llamada asíncrona.
    - *View Events*: Sirve para comunicar estados iniciados por el usuario. Por ejemplo un click sobre un botón.
    - *View Effects*: Sirve para comunicar estados no persistentes en la UI. Por ejemplo mostrar un *"toast"* con un mensaje indicativo.
- **LCE** *(Loading, Content, Error)*: Todos los casos de uso de la aplicación se definen mediante interfaces. Cada  interfaz deberá indicar que la operación que se va a ejecutar es de tipo suspendida "suspend". Para facilitar el manejo de error y mantener una comunicación robusta se hace uso de LCE, se trata de un patrón que permite indicar en que momento se encuentra la la llamada al repositorio: Loading, Error o Content:
    - Loading: Permite indicar que se ha empezado con la llamada al servicio.
    - Error: Permite indicar que se ha producido un error durante o al terminar la llamada al servicio. Dicho error se declara como una jerarquía para poder diferenciar los errores producidos por la red (no hay conexión, errores devueltos por backend, etc)  o una exception.
    - Content: Permite indicar que los datos se han obtenido de forma correcta.
- Para tener una separación total entre la capa del dominio y la de repositorio en remoto como puede ser Retrofit. Se hace uso de las funciones inline de Kotlin para devolver directamente un objeto Lce<T>, ya que en caso contrario se progaría objetos propios de Retrofit a la capa del viewModel.


## Features

- Poder acceder al listado de partidas de un usuario del servidor de EUW.

## Próximas Features
- Añadir más datos como las imágenes de los summoners spells, enemigos y aliados de la partida.
- Volcar imágenes a un backend propio para poder visualizarlas dentro de la app.
- Detalle de una partida con gráficas sobre el daño y oro.
- Añadir animaciones con MotionLayout.
- Agregar Jetpack Compose al proyecto.


>README en construción 👷🏽‍♂️
